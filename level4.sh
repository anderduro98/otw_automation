

#!/bin/bash

#Server details
server_ip="bandit.labs.overthewire.org"
username="bandit3"
password="aBZ0W5EmUfAf7kHTQeOwd8bauFJ2lAiG"
port="2220"

#SSH into the server with the specified username ans pass
sshpass -p $password ssh "$username"@"$server_ip" -p "$port" << EOF
    echo "the Key is "
    cat ./.hidden
    exit
EOF
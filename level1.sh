#!/bin/bash

#Server details
server_ip="bandit.labs.overthewire.org"
username="bandit0"
password="bandit0"

#SSH into the server with the specified username ans pass
sshpass -p $password ssh "$username"@"$server_ip" -p 2220 << EOF
    echo "the Key is "
    cat readme
    exit
EOF